use sample_stats::SampleStats;

fn main() {
    let mut s = SampleStats::new();
    for i in 1..1_000 {
        println!("{i}");
        for j in 1..1_000 {
            s.update(rand::random::<f64>());
        }
        println!("{}", s);
        println!("{:?}", s);
    }
}
