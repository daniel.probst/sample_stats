// Sample stats for monte carlo
//
/// sample statistics struct to manage  average and standard error of a sample of f64s
///
/// The struct SampleStats is  used to track number, sum and sum of squared samples to be able to
/// easily calculate the mean of a sample including it's standard error. The primary usage s for
/// convergence tracking in monte carlo simulations. The advantage of
/// keeping interim statistics in this way is the trivial aggregation of parallel simulations.
///
///
#[derive(Copy, Clone)]
pub struct SampleStats {
    /// the sample size
    pub n: usize,
    /// the mean of sample values
    pub mean: f64,
    /// the sum of sample values
    pub sum: f64,
    /// the sum of squares
    pub ss: f64,
    /// the sum of squared diffs from mean
    pub ssd: f64,
}

impl std::fmt::Display for SampleStats {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "n:{}, mean:{}, sum_ssd:{}", self.n, self.mean, self.ssd,)
    }
}

impl std::fmt::Debug for SampleStats {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let (mean, var, sem) = self.eval();
        write!(
            f,
            "n:{}, sum:{}, sum_sq:{}, mean:{}, var:{}, stderr_mean:{}",
            self.n, self.sum, self.ss, mean, var, sem
        )
    }
}

impl SampleStats {
    /// instantiate a new and empty version of SampleStats
    pub fn new() -> SampleStats {
        SampleStats {
            n: 0,
            mean: 0.0,
            sum: 0.0,
            ss: 0.0,
            ssd: 0.0,
        }
    }
    /// initialize a new SampleStats struct based on an initial observation
    pub fn init(sample: f64) -> SampleStats {
        SampleStats {
            n: 1,
            mean: sample,
            sum: sample,
            ss: sample * sample,
            ssd: 0.0,
        }
    }
    ///  update the sample stats with one new observation
    pub fn update(&mut self, sample: f64) {
        self.n += 1;
        let old_mean = self.mean;
        self.mean += (sample - self.mean) / self.n as f64;
        self.sum += sample;
        self.ss += sample * sample;
        self.ssd += (sample - self.mean) * (sample - old_mean);
    }
    /// evaluate the SampleStat struct and return sample mean, sample variance and standard error
    /// (the standard deviation of the sample mean)
    pub fn eval(&self) -> (f64, f64, f64) {
        if self.n == 0 {
            panic!(
                "Cannot eval a SampleStruct with 0 samples. This function call should never occur."
            );
        }
        let avg = self.sum / self.n as f64;
        let var = self.ss / self.n as f64 - avg * avg;
        let sem = f64::sqrt(var / self.n as f64);
        (avg, var, sem)
    }
    /// merge two SampleStats structs, returning a new struct
    pub fn merge(s1: &SampleStats, s2: &SampleStats) -> SampleStats {
        SampleStats {
            n: s1.n + s2.n,
            sum: s1.sum + s2.sum,
            ss: s1.ss + s2.ss,
        }
    }
    /// update a SampleStats structs by adding the stats of another struct
    pub fn add(&mut self, s1: &SampleStats) {
        self.n += s1.n;
        self.sum += s1.sum;
        self.ss += s1.ss;
    }
}

// TESTS

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(4, 4);
    }
}
